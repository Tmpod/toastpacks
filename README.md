[![Matrix room badge](https://img.shields.io/badge/Matrix-%23toastpacks%3Amatrix.org-0DBD8B?style=flat-square&logo=matrix)](https://matrix.to/#/#toastpacks:matrix.org)

# ToastPacks

This is a little collection of some datapacks I've written for Minecraft, with the revamped 1.13+
command system.

Each pack follows a similar directory layout: `internal` is used for "private" functions which a
user should never have to call, `internal/tick` is the main pack's loop, and `internal/load` is the
pack's entrypoint.  Tags, scoreboards and fake players also follow a similar naming scheme:
dot-notation for a hierarchy.  Due to some unfortunate constraints, some scoreboard names may have
abbreviations.

Check each datapack directory for more information on specifics.


## Found some pesky bug? Do you have some suggestion?

Please open an ticket in the issue tracker [here](https://gitlab.com/Tmpod/toastpacks/issues).


## Want to contribute a patch?

Open a merge request [here](https://gitlab.com/Tmpod/toastpacks/merge_requests) and will review it
as soon as I can. :slight_smile:


## Reach out to me

You can directly contact me via Discord - `Tmpod#2525` - or via Matrix - `@tmpod:matrix.org`.
I've also created a Matrix room 
- [`#toastpacks:matrix.org`](https://matrix.to/#/#toastpacks:matrix.org) - 
where group discussion can happen.


## License

All these datapacks are licensed under the GNU Lesser General Pulic License version 3 (LGPL v3).
The license file can be found [here](./COPYING.md) and its lesser complement
[here](./COPYING.LESSER.md).  Addionally, each source file has a header.
