# raio

A super simple and lightweight raio casting datapack.

The name is the portuguese word for "ray".


## Usage

Call the `raio:cast` function and then check for the nearest entity with the `raio.hit` tag.
This is not exactly the best method considering a multiplayer scenario, but it is rather simple and effective.
I may work on a multiplayer friendly approach in the future (possibly with the [`uuid`](../uuid) datapack).

I'm was also experimenting with casts stopping on entities.
