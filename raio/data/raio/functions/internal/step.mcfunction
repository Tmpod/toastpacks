# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

scoreboard players add @s raio.casts 1

execute unless block ~ ~ ~ minecraft:air run scoreboard players set @s raio.casts 2001
# execute as @s[tag=raio.stop_on_entity] if entity @e[type=!area_affect_cloud, distance=..0.4] run scoreboard players set @s raio.casts 1001

execute as @s[scores={raio.casts=2001}] run summon minecraft:area_effect_cloud ~ ~ ~ {Duration: 1, Radius: 0, Tags: ["raio.hit"]}
execute as @s[scores={raio.casts=..2000}] positioned ^ ^ ^0.06 run function raio:internal/step
