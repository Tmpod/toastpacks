# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

scoreboard objectives add uuid.ids dummy "UUIDs"
scoreboard objectives add uuid.base dummy
scoreboard objectives add uuid.timer dummy
scoreboard objectives add uuid.get trigger
scoreboard objectives add uuid.options dummy

execute unless score uuid.master_player uuid.base matches 1.. run scoreboard players set uuid.master_player uuid.base 1
execute unless score uuid.master_player uuid.timer matches 1.. run scoreboard players set uuid.master_player uuid.timer 0

# Default options values
execute unless score uuid.options.timer uuid.options matches 1.. run scoreboard players set uuid.options.timer uuid.options 200

scoreboard players enable @a uuid.get
