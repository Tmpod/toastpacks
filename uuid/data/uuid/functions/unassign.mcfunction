# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Notify first, then remove, so we can access the old UUIDs in the tellraw
# Subscribers
execute as @s[tag=uuid.assigned] run tellraw @a[tag=uuid.sub] [{"text": "UUID ", "color": "dark_gray"}, {"score": {"name": "@s", "objective": "uuid.ids"}, "color": "gray", "bold": true}, {"text": " unassigned from ", "color": "dark_gray"}, {"selector": "@s", "color": "gray", "bold": true}]

# Player
execute as @s[tag=uuid.assigned] run tellraw @s [{"text": "Your UUID (", "color": "dark_gray", "clickEvent": {"action": "suggest_command", "value": "/trigger uuid.get"}, "hoverEvent": {"action": "show_text", "contents": {"text": "If you believe this is an error please run /trigger uuid.get", "italic": true}}}, {"score": {"name": "@s", "objective": "uuid.ids"}, "bold": true, "color": "gray", "clickEvent": {"action": "suggest_command", "value": "/trigger uuid.get"}, "hoverEvent": {"action": "show_text", "contents": {"text": "If you believe this is an error please run /trigger uuid.get", "italic": true}}}, {"text": ") was unassigned", "color": "dark_gray", "clickEvent": {"action": "suggest_command", "value": "/trigger uuid.get"}, "hoverEvent": {"action": "show_text", "contents": {"text": "If you believe this is an error please run /trigger uuid.get", "italic": true}}}]

# Not actually resetting because this way we have a way of knowing if the UUID was reset
scoreboard players set @s[tag=uuid.assigned] uuid.ids -1
tag @s[tag=uuid.assigned] remove uuid.assigned


# Expanded JSON
# Subs:
# [
#   {
#     "text": "UUID ",
#     "color": "dark_gray"
#   },
#   {
#     "score": {
#       "name": "@s",
#       "objective": "uuid.ids"
#     },
#     "color": "gray",
#     "bold": true
#   },
#   {
#     "text": " unassigned from ",
#     "color": "dark_gray"
#   },
#   {
#     "selector": "@s",
#     "color": "gray",
#     "bold": true
#   }
# ]
# 
# Players:
# [
#   {
#     "text": "Your UUID (",
#     "color": "dark_gray",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger uuid.get"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "If you believe this is an error please run /trigger uuid.get",
#         "italic": true
#       }
#     }
#   },
#   {
#     "score": {
#       "name": "@s",
#       "objective": "uuid.ids"
#     },
#     "bold": true,
#     "color": "gray",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger uuid.get"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "If you believe this is an error please run /trigger uuid.get",
#         "italic": true
#       }
#     }
#   },
#   {
#     "text": ") was unassigned",
#     "color": "dark_gray",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger uuid.get"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "If you believe this is an error please run /trigger uuid.get",
#         "italic": true
#       }
#     }
#   }
# ]