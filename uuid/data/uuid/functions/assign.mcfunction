# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# I know the tag check is redundant when the function is called from uuid:internal/step, 
# but since this is part of the public API, we gotta do some validation
execute as @s[tag=!uuid.assigned] run scoreboard players operation @s uuid.ids = uuid.master_player uuid.base
function uuid:internal/increment_base

# Notify subscribers
tellraw @a[tag=uuid.sub] [{"text": "UUID ", "color": "dark_gray"}, {"score": {"name": "@s", "objective": "uuid.ids"}, "color": "gray", "bold": true}, {"text": " issued to ", "color": "dark_gray"}, {"selector": "@s", "color": "gray", "bold": true}]

# Notify player
tellraw @s[tag=!uuid.assigned] [{"text": "You just got assigned the UUID ", "color": "dark_gray", "clickEvent": {"action": "suggest_command", "value": "/trigger uuid.get"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You don't need to memorize it. You can get it anytime with /trigger uuid.get", "italic": true}}}, {"score": {"name": "@s", "objective": "uuid.ids"}, "bold": true, "color": "gray", "clickEvent": {"action": "suggest_command", "value": "/trigger uuid.get"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You don't need to memorize it. You can get it anytime with /trigger uuid.get", "italic": true}}}]

tag @s[tag=!uuid.assigned] add uuid.assigned

# Expanded JSON
# Subs:
# [
#   {
#     "text": "UUID ",
#     "color": "dark_gray"
#   },
#   {
#     "score": {
#       "name": "@s",
#       "objective": "uuid.ids"
#     },
#     "color": "gray",
#     "bold": true
#   },
#   {
#     "text": " was issued to ",
#     "color": "dark_gray"
#   },
#   {
#     "selector": "@s",
#     "color": "gray",
#     "bold": true
#   }
# ]
# 
# Player:
# [
#   {
#     "text": "You just got assigned the UUID ",
#     "color": "dark_gray",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger uuid.get"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You don't need to memorize it. You can get it anytime with /trigger uuid.get",
#         "italic": true
#       }
#     }
#   },
#   {
#     "score": {
#       "name": "@s",
#       "objective": "uuid.ids"
#     },
#     "bold": true,
#     "color": "dark_gray",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger uuid.get"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You don't need to memorize it. You can get it anytime with /trigger uuid.get",
#		  "italic": true
#       }
#     }
#   }
# ]
