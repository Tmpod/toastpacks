# uuid

A simple and straightforward UUID manager datapack.

This pack runs a function every 200 seconds (~3.3 minutes) that assigns a unique ID to every player if they do not have
one already.
Whenever this happens, each affected player gets a little message letting them know of the operation.
Optionally, OPs may sub/unsub from the operation feed using the `uuid:sub`/`uuid:unsub` functions, respectively.

All users can use the `uuid.get` trigger to get query their own UUID.


## Usage

* `/trigger uuid.get` / `/function uuid:get` - query the context entity's current UUID. If it does not have it yet, one will be assigned.
* `/function uuid:assign` - assign a UUID to the context entity if it does not have one already.
* `/function uuid:unassign` - removes the UUID from the context entity
* `/function uuid:sub` - subscribe to UUID (un)assignment feed.
* `/function uuid:unsub` - unsubscribe to UUID (un)assignment feed. These two commands might be merged into just one with "toggling" behaviour in 
the future.

