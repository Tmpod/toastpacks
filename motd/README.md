![WIP](https://img.shields.io/badge/%E2%9A%A0%EF%B8%8F-WIP-critical?style=for-the-badge)

# motd

A Message-Of-The-Day datapack with easy configuration.

This pack allows users to subscribe to your server's MOTD and to member join alerts.

I'm still working on a way of easily customising the MOTD, using a lecturn.
Don't know if scores work properly this way, I have to investigate further.
