# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

execute if score motd.options.toggle motd.options matches 1 as @a[scores={motd.joined=1}] run function motd:internal/show
execute if score motd.options.toggle motd.options matches 1 as @a[scores={motd.joined=1}] run function motd:internal/notify
execute if score motd.options.toggle motd.options matches 1 as @a[scores={motd.joined=1}] run schedule function motd:internal/notify2 10t append
execute as @a[scores={motd.show=1}] run function motd:show
execute as @a[scores={motd.sub=1}] run function motd:sub

scoreboard players set @a[scores={motd.joined=1}] motd.joined 0
