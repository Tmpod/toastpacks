# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

kill @e[tag=motd.marker]

function raio:cast
execute at @e[tag=raio.hit] run summon minecraft:area_effect_cloud ~ ~ ~ {Duration: 9999999999d, Radius: 0, Tags: ["motd.marker"]}

execute as @e[tag=motd.marker] at @s run data modify entity @s CustomName set from block ~ ~ ~ Book.tag.pages[0]
