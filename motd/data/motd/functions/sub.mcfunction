# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

scoreboard players set @s motd.sub 0
scoreboard players enable @s motd.sub

execute as @s[tag=!motd.sub, tag=!motd.dirty] run function motd:internal/sub
execute as @s[tag=motd.sub, tag=!motd.dirty] run function motd:internal/unsub
tag @s remove motd.dirty

execute as @s[tag=motd.sub] run tellraw @s [{"text": "Subscribed to join/leave events", "color": "dark_gray", "italic": true}]
execute as @s[tag=!motd.sub] run tellraw @s [{"text": "Unsubscribed to join/leave events", "color": "dark_gray", "italic": true}]
