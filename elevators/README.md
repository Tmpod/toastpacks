[![Depends on raio](https://img.shields.io/badge/Depends_on-raio-important?style=flat-square)](../raio)

# elevators

A vanilla datapack that loosely recreates [OpenBlock's Elevator mod](https://ftb.gamepedia.com/Elevator_(OpenBlocks)).


## Usage

### Creating an elevator 

To create an elevator, you simply have to drop an ender pearl on top of a wool block.
If the creation was successful, some particles will be emitted.
Breaking an elevator block will drop an ender pearl along with the broken block itself.

### Navigating

To go up simply jump, and sneak to go down.
Holding down sneak won't make you continuously teleport to the elevator below,
as a way to prevent accidental spam. 
However, you can still go down multiple levels really quickly, by quickly tapping the sneak button.

A slighly different sound will play in case you hit one of the boundaries,
in other words, if you try to go up on the highest elevator or down on the lowest elevator.

### Frequencies

Frequencies are a way for you to create more complex elevator structures,
by letting you have multiple independent networks.

In short, elevators can only see other elevators operating in the same frequency.

By default, differently coloured wool blocks have different frequencies.
White, orange, magenta, light blue, yellow, lime, pink, gray, light gray, cyan, purple, blue, brown, green, red, black, have frequencies from 1 to 16, respectively (ordered like they appear in the creative inventory).

However, you can change an elevator's frequency at any time, in case you want to have multicoloured networks.
You just have to look at an elevator block and use the `elevators.modfrq` trigger and set it to the value you want.

Example:

```mcfunction
/trigger elevators.modfrq set 1337
```

This will set the frequency of the elevator you are looking at to `1337`.

Additionally, you can get the frequency value for any elevator, by looking at it and triggering `elevators.getfrq`.

Example:

```mcfunction
/trigger elevators.getfrq
```


## API (for technical users)

At the moment, the datapack purposefully "exposes" both `elevators.in_up_tp` and `elevators.in_down_tp` tags for 1 tick, 
which are applied to players whenever they are travelling up or down, respectively.

Elevators are 0-width Area Effect Clouds with a `elevators.marker` tag.

The list of blocks allowed to be used as elevators consists of all wool colours by default, 
however it can be changed by overriding the `#elevators:allowed_blocks` block tag.

The default frequency for blocks can also be changed, by overriding the `#elevators:frequency_<n>`, where `n` is the frequency value.
At the moment, only values from `1` to `16` (inclusive) are affected by this,
but the limit may be raised in the future.
Alternatively, you can override the `elevators:internal/assign_frequency` function to test for more tags.
