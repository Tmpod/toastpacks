# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

scoreboard objectives add elevators.jumps minecraft.custom:jump
scoreboard objectives add elevators.sneak minecraft.custom:sneak_time
scoreboard objectives add elevators.psneak minecraft.custom:sneak_time
scoreboard objectives add elevators.freqs dummy
scoreboard objectives add elevators.modfrq trigger
scoreboard objectives add elevators.getfrq trigger

scoreboard players enable @a elevators.modfrq 
scoreboard players enable @a elevators.getfrq
