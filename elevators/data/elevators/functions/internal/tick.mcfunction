# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

tag @e[tag=elevators.in_up_tp] remove elevators.in_up_tp
tag @e[tag=elevators.in_down_tp] remove elevators.in_down_tp

function elevators:internal/check_destroy
function elevators:internal/check_pearl

function elevators:internal/check_jump
function elevators:internal/check_sneak

scoreboard players set @a[scores={elevators.sneak=0, elevators.psneak=1..}] elevators.psneak 0
scoreboard players set @a[scores={elevators.jumps=1..}] elevators.jumps 0
scoreboard players set @a[scores={elevators.sneak=1..}] elevators.sneak 0

execute as @a[scores={elevators.modfrq=1..}] run function elevators:internal/modfrq
execute as @a[scores={elevators.getfrq=1..}] run function elevators:internal/getfrq
