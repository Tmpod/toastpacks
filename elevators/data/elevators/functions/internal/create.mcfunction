# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# End the setup permaturely if some conditions are not met
execute if block ~ ~ ~ #elevators:allowed_blocks align xyz unless entity @e[tag=elevators.marker, dx=0, dy=0, dz=0] run summon minecraft:area_effect_cloud ~0.5 ~ ~0.5 {Duration: 999999999999d, Radius: 0, Tags: ["elevators.setup_tmp"]}

execute as @e[tag=elevators.setup_tmp] at @s run function elevators:internal/assign_frequency

execute at @e[tag=elevators.setup_tmp] run particle minecraft:reverse_portal ~ ~0.8 ~ 0.1 0.1 0.1 0.05 100 force

tag @e[tag=elevators.setup_tmp] add elevators.marker
tag @e[tag=elevators.setup_tmp] remove elevators.setup_tmp
