# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

execute as @a[scores={elevators.sneak=1, elevators.psneak=1}, nbt={OnGround:1b}] at @s positioned ~ ~-1.5 ~ if entity @e[distance=..1, tag=elevators.marker] run tag @s add elevators.in_down_tp

execute at @a[tag=elevators.in_down_tp] anchored feet align xyz positioned ~ ~-2 ~ as @e[tag=elevators.marker, dx=0, dy=-15, dz=0, limit=1, sort=nearest] if score @s elevators.freqs = @e[tag=elevators.marker, dx=0, dy=1, dz=0, limit=1, sort=nearest] elevators.freqs at @s run summon minecraft:area_effect_cloud ~ ~1 ~ {Duration: 1, Radius: 0, Tags: ["elevators.tp_target"]}

execute as @a[tag=elevators.in_down_tp] at @s align xyz positioned ~ ~-1 ~ run data modify entity @e[tag=elevators.tp_target, dx=0, dy=-15, dz=0, limit=1, sort=nearest] Pos[0] set from entity @s Pos[0]
execute as @a[tag=elevators.in_down_tp] at @s align xyz positioned ~ ~-1 ~ run data modify entity @e[tag=elevators.tp_target, dx=0, dy=-15, dz=0, limit=1, sort=nearest] Pos[2] set from entity @s Pos[2]

execute as @a[tag=elevators.in_down_tp] at @s align xyz positioned ~ ~-1 ~ at @e[tag=elevators.tp_target, dx=0, dy=-15, dz=0, limit=1, sort=nearest] run teleport @s ~ ~ ~

execute at @a[tag=elevators.in_down_tp] if entity @e[tag=elevators.tp_target, distance=..1] run playsound minecraft:ui.toast.in player @a ~ ~ ~ 1 2
execute at @a[tag=elevators.in_down_tp] unless entity @e[tag=elevators.tp_target, distance=..1] run playsound minecraft:ui.toast.out player @a ~ ~ ~ 1 1.8
