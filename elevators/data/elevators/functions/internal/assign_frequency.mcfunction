# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Default frequency
scoreboard players set @s elevators.freqs 42

# Cheesy but effective
execute if block ~ ~ ~ #elevators:frequency_1 run scoreboard players set @s elevators.freqs 1
execute if block ~ ~ ~ #elevators:frequency_2 run scoreboard players set @s elevators.freqs 2
execute if block ~ ~ ~ #elevators:frequency_3 run scoreboard players set @s elevators.freqs 3
execute if block ~ ~ ~ #elevators:frequency_4 run scoreboard players set @s elevators.freqs 4
execute if block ~ ~ ~ #elevators:frequency_5 run scoreboard players set @s elevators.freqs 5
execute if block ~ ~ ~ #elevators:frequency_6 run scoreboard players set @s elevators.freqs 6
execute if block ~ ~ ~ #elevators:frequency_7 run scoreboard players set @s elevators.freqs 7
execute if block ~ ~ ~ #elevators:frequency_8 run scoreboard players set @s elevators.freqs 8
execute if block ~ ~ ~ #elevators:frequency_9 run scoreboard players set @s elevators.freqs 9
execute if block ~ ~ ~ #elevators:frequency_10 run scoreboard players set @s elevators.freqs 10
execute if block ~ ~ ~ #elevators:frequency_11 run scoreboard players set @s elevators.freqs 11
execute if block ~ ~ ~ #elevators:frequency_12 run scoreboard players set @s elevators.freqs 12
execute if block ~ ~ ~ #elevators:frequency_13 run scoreboard players set @s elevators.freqs 13
execute if block ~ ~ ~ #elevators:frequency_14 run scoreboard players set @s elevators.freqs 14
execute if block ~ ~ ~ #elevators:frequency_15 run scoreboard players set @s elevators.freqs 15
execute if block ~ ~ ~ #elevators:frequency_16 run scoreboard players set @s elevators.freqs 16
