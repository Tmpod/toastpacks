# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Error message
execute align xyz unless entity @e[tag=elevators.marker, dx=0, dy=0, dz=0] run tellraw @s [{"text": "You did not point to an elevator!", "color": "red"}]

execute align xyz if entity @e[tag=elevators.marker, dx=0, dy=0, dz=0] run tellraw @s [{"text": "This elevator's frequency is ", "color": "aqua", "clickEvent": {"action": "suggest_command", "value": "/trigger elevators.modfrq set <value>"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You can always change it by using /trigger elevators.modfrq set <value>", "italic": true}}}, {"score": {"name": "@e[tag=elevators.marker, dx=0, dy=0, dz=0, limit=1]", "objective": "elevators.freqs"}, "color": "gold", "bold": true, "clickEvent": {"action": "suggest_command", "value": "/trigger elevators.modfrq set <value>"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You can always change it by using /trigger elevators.modfrq set <value>", "italic": true}}}]

# Expanded JSON
# [
#   {
#     "text": "This elevator's frequency is ",
#     "color": "aqua",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger elevators.modfrq set <value>"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You can always change it by using /trigger elevators.modfrq set <value>",
#         "italic": true
#       }
#     }
#   },
#   {
#     "score": {
#       "name": "@e[tag=elevators.marker, dx=0, dy=0, dz=0, limit=1]",
#       "objective": "elevators.freqs"
#     },
#     "color": "gold",
#     "bold": true,
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger elevators.modfrq set <value>"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You can always change it by using /trigger elevators.modfrq set <value>",
#         "italic": true
#       }
#     }
#   }
# ]
