# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Error message
execute align xyz unless entity @e[tag=elevators.marker, dx=0, dy=0, dz=0] run tellraw @s [{"text": "You did not point to an elevator!", "color": "red"}]

# Since we need the entity context to be the changer so we can get the new frequency from the scoreboard, 
# we have to work with the positional context
# The align and volume check is somewhat unecessary, but doesn't hurt to ensure we really get the right marker.
execute align xyz as @e[tag=elevators.marker, dx=0, dy=0, dz=0, limit=1, sort=nearest] run tag @s add elevators.modfrq_tmp
execute align xyz if entity @e[tag=elevators.marker, dx=0, dy=0, dz=0, limit=1, sort=nearest] run tag @s add elevators.modfrq_tmp_p

tellraw @s[tag=elevators.modfrq_tmp_p] [{"text": "Successfully changed the target elevator's frequency from ", "color": "aqua", "clickEvent": {"action": "suggest_command", "value": "/trigger elevators.getfrq"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You can always get it again by using /trigger elevators.getfrq", "italic": true}}}, {"score": {"name": "@e[tag=elevators.modfrq_tmp, limit=1]", "objective": "elevators.freqs"}, "color": "gold", "bold": true, "clickEvent": {"action": "suggest_command", "value": "/trigger elevators.getfrq"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You can always get it again by using /trigger elevators.getfrq", "italic": true}}}, {"text": " to ", "color": "aqua"}, {"score": {"name": "@s", "objective": "elevators.modfrq"}, "color": "gold", "bold": true, "clickEvent": {"action": "suggest_command", "value": "/trigger elevators.getfrq"}, "hoverEvent": {"action": "show_text", "contents": {"text": "You can always get it again by using /trigger elevators.getfrq", "italic": true}}}]

execute as @e[tag=elevators.modfrq_tmp] run scoreboard players operation @s elevators.freqs = @p[tag=elevators.modfrq_tmp_p] elevators.modfrq

scoreboard players set @s elevators.modfrq 0
tag @e[tag=elevators.modfrq_tmp] remove elevators.modfrq_tmp
tag @e[tag=elevators.modfrq_tmp_p] remove elevators.modfrq_tmp_p

# Expanded JSON
# [
#   {
#     "text": "Successfully changed the target elevator's frequency from ",
#     "color": "aqua",
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger elevators.getfrq"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You can always get it again by using /trigger elevators.getfrq",
#         "italic": true
#       }
#     }
#   },
#   {
#     "score": {
#       "name": "@e[tag=elevators.modfrq_tmp, limit=1]",
#       "objective": "elevators.freqs"
#     },
#     "color": "gold",
#     "bold": true,
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger elevators.getfrq"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You can always get it again by using /trigger elevators.getfrq",
#         "italic": true
#       }
#     }
#   },
#   {
#     "text": " to ",
#     "color": "aqua"
#   },
#   {
#     "score": {
#       "name": "@s",
#       "objective": "elevators.modfrq"
#     },
#     "color": "gold",
#     "bold": true,
#     "clickEvent": {
#       "action": "suggest_command",
#       "value": "/trigger elevators.getfrq"
#     },
#     "hoverEvent": {
#       "action": "show_text",
#       "contents": {
#         "text": "You can always get it again by using /trigger elevators.getfrq",
#         "italic": true
#       }
#     }
#   }
# ]
