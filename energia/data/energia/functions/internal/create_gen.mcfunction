# This file is part of TostaPacks, a collection of Minecraft datapacks.
# Copyright (C) 2020  Tmpod
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# End the setup permaturely if some conditions are not met
execute if block ~ ~ ~ minecraft:end_portal_frame run summon minecraft:area_effect_cloud ~ ~ ~ {Duration: 999999999999d, Radius: 0, Tags: ["energia.gen_setup"]}

execute as @e[tag=energia.gen_setup] at @s align xyz if entity @e[tag=energia.gen_marker, dx=1, dy=1, dz=1] run kill @s

execute as @e[tag=energia.gen_setup] at @s align xyz run tp ~0.5 ~ ~0.5

execute at @e[tag=energia.gen_setup] run function energia:internal/play_gen_create_particles
execute at @e[tag=energia.gen_setup] run function energia:internal/play_gen_create_sounds

scoreboard players operation @e[tag=energia.gen_setup] energia.owner = @s uuid.ids
tag @e[tag=energia.gen_setup] add energia.gen_marker
tag @e[tag=energia.gen_setup] remove energia.gen_setup
