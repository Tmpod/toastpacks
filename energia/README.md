![WIP](https://img.shields.io/badge/%E2%9A%A0%EF%B8%8F-WIP-critical?style=for-the-badge)

# energia

A simple energy datapack, inspired by [Extra Utilities 2's Grid Power](https://ftb.gamepedia.com/Grid_Power),
with the intent of feeling as vanilla as possible.

The name is the portuguese word for "energy".
